<?php
    require_once('controllers/base_controller.php');
    require_once('models/user.php');
    require_once('lib/session.php');

    class UsersController extends BaseController
    {

        private $models_user;

        function __construct()
        {
            $this->folder = 'users';
            $this->models_user = new User();
        }

        public function getLogin()
        {
            $data = [];
            $this->render('login', $data);
        }

        public function postLogin()
        {

            $email = $_POST['email'];
            $password = $_POST['password'];

            $result = $this->models_user->checkLogin($email, $password);

            if ($result == "true") {
                $data = array('alert' => "");
                $this->render('index', $data);
            } else {
                $data = array('alert' => $result);
                $this->render('login', $data);
            }
        }

        public function logout()
        {
            Session::destroy();
            $data = [];
            $this->render('login', $data);
        }

        public function getRegister()
        {
            $data = [];
            $this->render('register', $data);
        }

        public function postRegister()
        {

            if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password_repeat'])) {
                $email = $_POST["email"];
                $password = $_POST["password"];
                $password_repeat = $_POST["password_repeat"];


                $dataRegister = array(
                    "email" => $email,
                    "password" => $password,
                    "password_repeat" => $password_repeat,
                    "level" => "0",
                );

                $result = $this->models_user->add($dataRegister);

                if ($result == "true") {
                    $data = array('alert' => "");
                    $this->render('index', $data);
                } else {
                    $data = array('alert' => $result);
                    $this->render('register', $data);
                }
            }
        }
    }
