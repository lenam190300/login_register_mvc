-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 17, 2021 at 05:32 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intern_demo_mvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `level`) VALUES
(83, 'lenam190300@gmail.comyt', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(84, 'lenam190300@gmail.comttt', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(85, 'lenam190300@gmail.comgggs', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(86, 'lenam190300@gmail.com44aa', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(87, 'lenam190300@gmail.com312a', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(88, 'lenam190300@gmail.com112r', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(89, 'lenam190300@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(90, 'lenam190300@gmail.com2', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(91, 'ldpnam1004@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(92, 'lenam190300@gmail.com44', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(93, 'lenam190300@gmail.coma', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 0),
(94, 'lenam190300@gmail.com3a', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 0),
(95, 'lenam190300@gmail.comg', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(96, 'lenam190300@gmail.com41', 'a87ff679a2f3e71d9181a67b7542122c', 0),
(97, 'lenam190300@gmail.comff', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(98, 'lenam190300@gmail.comdd', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(99, 'lenam190300@gmail.comdd1', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(100, 'lenam190300@gmail.comdd1a', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(101, 'lenam190300@gmail.comdd1aa', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(102, '3lenam190300@gmail.comdd1aa', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(103, 'lenam190300@gmail.comgg', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(104, 'lenam190300@gmail.comtt', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(105, 'lenam190300@gmail.comtt1', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(106, 'lenam190300@gmail.comyy', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(107, 'lenam190300@gmail.comr1', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(108, 'lenam190300@gmail.comtt12', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(109, 'lenam190300@gmail.comtt123', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(110, 'lenam190300@gmail.com441', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(111, 'lenam190300@gmail.com41w', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(112, 'lenam190300@gmail.comq1', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(113, 'lenam190300@gmail.comt11', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(114, 'lenam190300@gmail.comt113', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 0),
(115, 'lenam190300@gmail.comqq', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 0),
(116, 'lenam190300@gmail.com5a', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 0),
(117, 'lenam190300@gmail.comq21', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 0),
(118, 'lenam190300@gmail.com3', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 0),
(119, 'lenam190300@gmail.com1r', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(120, 'lenam190300@gmail.com5q', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(121, 'lenam190300@gmail.com5aw', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(122, 'lenam190300@gmail.comt1', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(123, 'lenam190300@gmail.comgg1', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(124, 'lenam190300@gmail.com4t1', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(125, 'lenam190300@gmail.comt12', '182be0c5cdcd5072bb1864cdee4d3d6e', 0),
(126, 'lenam190300@gmail.comhn', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 0),
(127, 'lenam190300@gmail.comn', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 0),
(128, 'lenam190300@gmail.comnn', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(129, 'lenam190300@gmail.comvv', 'c4ca4238a0b923820dcc509a6f75849b', 0),
(130, 'lenam190300@gmail.com1gg', 'c4ca4238a0b923820dcc509a6f75849b', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
