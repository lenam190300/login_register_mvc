<?php
require_once('lib/database.php');
require_once('helpers/format.php');
require_once('lib/session.php');

class User
{
    private $conn;
    private $format;

    function __construct()
    {
        $this->format = new format();
        $this->conn = new Database();
    }

    public function checkLogin($email, $password)
    {

        $email = $this->format->validation($email);
        $password = $this->format->validation(md5($password));

        if (empty($email) || empty($password)) {
            $alert = "User and Password must be not empty";
            return $alert;
        } else {
            $query = "SELECT * FROM users WHERE email = '$email' AND password = '$password' LIMIT 1";
            $result = $this->conn->select($query);

            if ($result != false) {
                // Tìm nạp một hàng kết quả dưới dạng một mảng kết hợp
                $value = $result->fetch_assoc();
                Session::set('login', true);
                Session::set('id', $value['id']);
                Session::set('email', $value['email']);
                Session::set('level', $value['level']);
                return true;
            } else {
                $alert = "User and Pass not match";
                return $alert;
            }
        }
    }

    public function add($dataRegister)
    {
        
        $email = $this->format->validation($dataRegister['email']);
        $password = $this->format->validation(md5($dataRegister['password']));
        $password_repeat = $this->format->validation(md5($dataRegister['password_repeat']));
        $level = $dataRegister['level'];


        if (empty($email) || empty($password) || empty($password_repeat)) {
            $alert = "User, Password and Password repeat must be not empty";
            return $alert;
        } else {
            $query = "SELECT * FROM users WHERE email = '$email'  ";
            $result = $this->conn->select($query);


            if ($result != false) {

                $alert = "Email already exists!";
                return $alert;
            } else {
                if ($password_repeat == $password) {

                    $query = "INSERT INTO users (email, password, level) VALUES ('$email','$password','$level')";
                    $result = $this->conn->insert($query);
                    if ($result != false) {
                      
                        Session::set('login', true);
                        Session::set('email', $email);
                        Session::set('level', $level);
                        return $result;
                    } else {
                        $alert = "error insert";
                        return $alert;
                    }
           
                } else {

                    $alert = "Register failed!. Please check again.";
                    return $alert;
                }
            }
        }
    }
}
