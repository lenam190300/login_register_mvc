<?php
    require_once('lib/database.php');

    if (isset($_GET['controller'])) {
        $controller = $_GET['controller'];
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
        } else {
            $action = 'index';
        }
    } else {
        $controller = 'users';
        $action = 'getLogin';
    }
    require_once('routes.php');
