<form method="post" action="?controller=users&action=postRegister">
    <div class="register">
        <h1>Register</h1>
        <hr class="register__hr">
        <span>
            <?php
            if (isset($alert)) {
                echo $alert;
            }
            ?>
        </span>
        <label for="email"><b>Email</b></label>
        <input class="register__input" type="email" placeholder="Enter Email" name="email" id="email" value="" required>
        <label for="password"><b>Password</b></label>
        <input class="register__input" type="password" placeholder="Enter Password" name="password" id="password" value="" required>
        <label for="password-repeat"><b>Repeat Password</b></label>
        <input class="register__input" type="password" placeholder="Repeat Password" name="password_repeat" id="password-repeat" required>
        <hr class="register__hr">
        <button type="submit" class="button">Register</button>
        <div class="register__signin">
            <p>Already have an account? <a href="?controller=users&action=getLogin">Sign in</a>.</p>
        </div>
    </div>
</form>