<form method="post" action="?controller=users&action=postLogin">
    <div class="login">
        <h1>Login</h1>
        <hr class="login__hr">
        <span>
            <?php
            if (isset($alert)) {
                echo $alert;
            }
            ?>
        </span>
        <label for="email"><b>Email</b></label>
        <input class="login__input" type="email" placeholder="Enter Email" name="email" id="email" value="">
        <label for="password"><b>Password</b></label>
        <input class="login__input" type="password" placeholder="Enter Password" name="password" id="password" value="">
        <hr class="login__hr">
        <button type="submit" class="button">Login</button>
        <div class="login__register">
            <p>Already haven't an account? <a href="?controller=users&action=getRegister">Register now</a>.</p>
            <a href="#">Forgot password?</a>
        </div>
    </div>
</form>